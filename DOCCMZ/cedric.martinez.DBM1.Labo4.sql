-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
-- Partie 1 Définition de nouveaux opérateurs

-- Partie 2 Définition d'une alerte par rapport à un numéro d'erreur

-- Partie 3 Définition d'une alerte par rapport à une gravité d'erreur

-- Partie 4 Définition d'un nouveau travail

-- Partie 5 Planification de l'exécution d'un travail

-- Partie 6 Notification d'un opérateur suite à l'exécution du travail

-- Partie 7 Exécution d'un travail en réponse à une alerte

-- Partie 8 Définition d'une alerte liée à une condition de performance

-- Partie 9 Définition de compteurs de performances personnalisés

-- Partie 10 Définition d'une alerte par rapport à un compteur personnalisé
