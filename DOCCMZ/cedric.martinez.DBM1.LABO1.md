---
title: DBM1 - Documentation SQL Partie 1
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\sql-server.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify


# Questionnaire

## Quelle édition de SQL Server est disponible gratuitement ?

### Réponse

Les versions SQL Server Express et Developper.

## Quelles propriétés doit posséder le compte d'utilisateur Windows utilisé pour les services SQL Server ?

A. Appartenir au groupe des administrateurs du domaine
B. Appartenir au groupe des administrateurs du poste
C. Être un compte d'utilisateur défini sur le domaine
D. Être un compte d'utilisateur défini localement

### Réponse

D

## Si deux instances SQL Server sont installées sur le même poste, combien d'instances du service SQL Server Agent vont être définies:

A. 1
B. 2

### Réponse

2

## Le service d'indexation de texte intégral est-il commun à toutes les instances installées sur le poste ?

A. Oui
B. Non

### Réponse

A

## Quelles éditions de SQL Server permettent d'être abonné à un service de réplication ?

### Réponse

Toutes les éditions de SQL Server.

## Quel programme faut-il lancer pour installer une nouvelle instance de SQL Server?

A. Gestionnaire de programmes SQL Server
B. Ajout/suppression de programmes depuis le panneau de configuration
C. Setup.exe depuis le DVD d'installation de SQL Server
D. Menu Fichier - Nouveau - Instance de SQL Server Management Studio

### Réponse

C

## Quel est le mode de sécurité défini par défaut lors de l'installation d'une instance de SQL Server ?

A. Mode de sécurite Windows
B. Mode de sécurité SQL Server
C. Mode de sécurité mixte (Windows et SQL Server)

### Réponse

A

## Quelle base d'exemple est installée de façon automatique sur toute nouvelle instance de SQL Server 2016 ?

A. Aucune
B. Pubs
C. Northwind

### Réponse

A

\newpage

# Réalisation

## Choix de l'édition de SQL Server

La plateforme d'installation étant Windows Server 2016, la version de SQL Server qui serait adaptée serait la version 2017. Au point de vue des fonctionnalités l'édition Développer serait la plus adaptée puisqu'il n'y a aucune limitation de fonctionnalité et l'envrionnement de travail est un cadre de formation.

## Installation de l'instance

Après installlation de Windows Server 2016 et téléchargement du fichier d'installation de SQL Server 2017 édition Developper.

Il faut double-cliquer sur le fichier .msi.

Puis, il faut cliquer sur le bouton Basic afin de sélectionner l'installation avec les paramètres par défaut.

Ensuite, il faut cliquer sur le bouton Accept afin d'accepter la license.

Puis, il faut sélectionner l'emplacement d'installation de SQL Server et cliquer sur le bouton Install.

## Installation de la DB d'example

Il faut tout d'abord télécharger la DB AdvantureWorks qui est disponible en archive sur CodePlex.

Il faut ensuite ouvrir SSMS et se connecter à la base de données SQL Server depuis le serveur en utilisant l'authentification Windows en étant connecté avec le compte administrateur local.

Puis, il faut cliquer-droit sur le dossier Databases et sélectionner Restore Database.

Après cela, il faut sélectionner le radio-bouton Device.

Ensuite, il faut cliquer sur le bouton Browse, cliquer sur le bouton Add, sélectionner le fichier .bak et cliquer sur le bouton OK.

Puis, il faut confirmer la sélection du fichier en cliquant sur le bouton OK.

Finalement, il faut cliquer sur le bouton OK pour confirmer la restoration de la base données à partir du fichier .bak.

Puis, il faut extraire le fichier .zip dans le dossier C:\\Samples\\AdventureWorks\\ en cliquant-droit sur le fichier et en sélectionnant Extract All.

Ensuite, il faut ouvrir SSMS et se connecter à la base de données SQL Server depuis le serveur en utilisant l'authentification Windows en étant connecté avec le compte administrateur local.

Puis, il faut cliquer sur le menu File puis sur Open et sur File.

Ensuite, il faut sélectionner le fichier .sql situé dans le dossier extrait précédemment et cliquer sur le bouton Open.

Il faut après cela, cliquer sur le menu Query et cliquer sur SQLCMD Mode.

Puis, il faut cliquer sur le bouton Execute.

## Installation de l'instance nommée

Il faut tout d'abord ouvrir l'installateur de SQL Server en double-cliquant dessus.

Puis, il faut cliquer sur le bouton Custom.

Il faut sélectionner l'emplacement du média d'installation et cliquer sur le bouton Install.

Il faut ensuite cliquer sur le bouton Installation.

Puis, il faut ensuite sélectionner New SQL Server stand-alone installation or add features to an existing installation.

Il faut ensuite cliquer sur le bouton Next à l'écran de sélection du mode de mise à jour.

Après cela, il faut cliquer sur le bouton Next à l'écran de vérification des règles.

Ensuite, il faut cliquer sur Perform a new installation of SQL Server 2017 et cliquer sur le bouton Next.

Puis, il faut sélectionner Specify a free edition et cliquer sur le bouton Next.

Il faut ensuite cocher la case à cocher nommée I Accept the license terms et cliquer sur le bouton Next.

Puis, il faut cocher la case à cocher Database Engine Services et Full-Text and Semantic Extractions et cliquer sur le bouton Next.

Il faut ensuite saisir le nom de la nouvelle instance dans le champ Named instance et cliquer sur le bouton Next.

Puis, il faut cliquer sur le bouton Next à l'écran de sélection de la configuration de l'instance.

Ensuite, il faut cliquer sur le bouton Add et saisir le nom de l'utilisateur administrateur et cliquer sur le bouton Next.

Puis, il faut cliquer sur le bouton Install pour lancer l'installation.

Finalment, il faut cliquer sur le bouton Close.

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.


\newpage

# Annexes

Ment.
