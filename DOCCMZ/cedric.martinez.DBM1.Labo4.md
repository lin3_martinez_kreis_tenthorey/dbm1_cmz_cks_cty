---
title: DBM1 - Documentation SQL Partie 4
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\sql-server.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Questionnaire

1. Un opérateur correspond-il obligatoirement à un utilisateur de base de données ?

    - A. Oui
    - B. Non

Réponse: B
&nbsp;

2. Dans quelle base est stockée la définition des opérateurs et des travaux ?

    - A. Master.
    - B. Model
    - C. MSDB
    - D. DBAgent

Réponse: C
&nbsp;

3. Une alerte peut être liée à

    - A. Un numéro d'erreur
    - B. Une gravité de message d'erreur
    - C. Un seuil défini d'un compteur de performances

Réponse:
&nbsp;

4. Est-il possible de définir ses propres compteurs de performances ?

    - A. Oui
    - B. Non

Réponse: A
&nbsp;

5. Est-il possible de lier une alerte à une erreur si elle ne provoque pas une inscription dans l'observateur des événements ?

    - A. Oui.
    - B. Non

Réponse:
&nbsp;

6. Si une alerte est liée à un niveau de performance, est-il nécessaire que l'analyseur de performances s'exécute sur le serveur ?

    - A. Oui
    - B. Non

Réponse:
&nbsp;

7. Une alerte définie par rapport à une erreur est :

    - A. Valide pour toutes les bases du serveur
    - B. Valide pour une base du serveur
    - C. Valide pour une ou plusieurs bases de données du serveur

Réponse:
&nbsp;

8. Dans quel cas un travail peut-il notifier un opérateur ?

    - A. Lorsque les tâches se sont exécutées avec succès
    - B. Lorsqu'une ou plusieurs tâches n'ont pu s'exécuter
    - C. Quelle que soit la réussite des tâches

Réponse: C
&nbsp;

9. Dans quel cas un travail peut-il être exécuté ?

    - A. A la demande de l'administrateur de base de données
    - B. A la suite du déclenchement d'une alerte
    - C. Par rapport à une planification prédéfinie
    - D. Dans tous ces cas

Réponse:
&nbsp;

10. Quel service se charge d'exécuter les travaux planifiés?

    - A. MS SQL Server
    - B. SQL Server Agent
    - C. MSDTC.
    - D. MS Search

Réponse:

\newpage

# Laboratoire

## Définition de nouveaux opérateurs

### Définition de l'opératrice Marie

Afin de définir l'opératrice Marie, il est nécessaire de lancer SSMS et de se connecter à l'instance.

Puis, il faut cliquer sur le dossier SQL Server Agent, puis cliquer-droit sur le dossier Operators et sélectionner New operator.

Ensuite, il faut saisir le nom de l'opérateur dans le champ Name.

Après cela, il faut saisir l'adresse e-mail dans le champ e-mail name.

Finalement, il faut cliquer sur le bouton OK.

### Définition de l'opérateur Philippe

Afin de définir l'opérateur Philippe en utilisant un script, il est nécessaire de lancer SSMS et de se connecter à l'instance.

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
-- Partie 1 Définition de nouveaux opérateurs
USE "msdb" ;  
GO  

EXEC dbo.sp_add_operator  
    @name = N'Philippe',  
    @enabled = 1,  
    @email_address = N'cedric.martinez@cpnv.ch';  
GO  
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage des opérateurs | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition d'une alerte par rapport à un numéro d'erreur

### Définition de l'alerte Erreur701SSMS

Afin de définir l'alerte Erreur701SSMS en utilisant SSMS, il est nécessaire de lancer SSMS et de se connecter à l'instance.

Puis, il faut cliquer sur le dossier SQL Server Agent, puis cliquer-droit sur le dossier Alerts et sélectionner New Alert.

Ensuite, il faut saisir le nom de l'alerte dans le champ Name.

Après cela, il faut sélectionner la base de données SSMS dans le champ Database name.

Suite à cela, il faut sélectionner le radio-bouton Error number et saisir 701.

Puis, il faut cliquer sur le lien Response dans le menu de gauche.

Il faut ensuite cocher la case à cocher Notify operators.

Après cela, il faut cocher la case à cocher e-mail à la ligne contenant l'opératrice Marie et l'opérateur Phillipe.

Suite à cela, il faut cliquer sur le lien Options dans le menu de gauche.

Il faut ensuite cocher la case à cocher E-mail du champ Include alert error test in ainsi que saisir 10 dans le champ Delay between responses.

Finalement, il faut cliquer sur le bouton OK.

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de l'alerte créée | OK |
| Réception des laertes sur l'adresse e-mail spécifiée | OK |


#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition d'une alerte par rapport à une gravité d'erreur

### Définition de l'alerte Gravite21TSQL

Afin de définir l'alerte Gravite21TSQL en utilisant un script, il est nécessaire de lancer SSMS et de se connecter à l'instance.

Puis, il faut cliquer sur le bouton New query et saisir le code ci-dessous.


```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
-- Partie 3 Définition d'une alerte par rapport à une gravité d'erreur

USE msdb ;  
GO  

EXEC dbo.sp_add_alert  
    @name = N'Gravite21TSQL',  
    @message_id = 0,   
   @severity = 21,   
   @include_event_description_in = 1,
   @database_name = "TSQL";  
GO  

EXEC dbo.sp_add_notification  
 @alert_name = N'Gravite21TSQL',  
 @operator_name = N'Marie',  
 @notification_method = 1 ;  
GO  

```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de l'alerte créée | OK |
| Réception des laertes sur l'adresse e-mail spécifiée | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition d'un nouveau travail

### Définition du travail Réduction

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4

USE msdb ;  
GO  
EXEC dbo.sp_add_job  
    @job_name = N'Réduction' ;  
GO  
EXEC sp_add_jobstep  
    @job_name = N'Réduction',  
    @step_name = N'Reduction taille DB TSQL',  
    @subsystem = N'TSQL',  
    @command = N'DBCC SHRINKDATABASE (TSQL, 10)',   
    @retry_attempts = 5,  
    @retry_interval = 5 ;  
GO  
EXEC sp_add_jobstep  
    @job_name = N'Réduction',  
    @step_name = N'Reduction taille DB SSMS',  
    @subsystem = N'TSQL',  
    @command = N'DBCC SHRINKDATABASE (SSMS, NOTRUNCATE)',   
    @retry_attempts = 5,  
    @retry_interval = 5 ;  
GO  
EXEC dbo.sp_add_jobserver  
    @job_name = N'Réduction';  
GO  
```

### Définition du travail SuiviErreur

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
USE msdb ;  
GO  
EXEC dbo.sp_add_job  
    @job_name = N'SuiviErreur' ;  
GO  
EXEC sp_add_jobstep  
    @job_name = N'SuiviErreur',  
    @step_name = N'Inscription dateheure actuelle dans TSQL.SuiviErreurs',  
    @subsystem = N'TSQL',  
    @command = N'USE "TSQL";
    IF OBJECT_ID (N"dbo.SuiviErreurs", N"U") IS NULL
    CREATE TABLE "SuiviErreurs"(
      id INT NOT NULL IDENTTIY(1,1) PRIMARY KEY,
      quand DATETIME2 DEFAULT);
    ELSE
    INSERT INTO [SuiviErreurs](
      [quand])
    VALUES (
      GETDATE());',   
    @retry_attempts = 5,
    @database_name= N'TSQL',
    @retry_interval = 5 ;  
GO  
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Planification de l'exécution d'un travail

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Notification d'un opérateur suite à l'exécution du travail

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Exécution d'un travail en réponse à une alerte

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition d'une alerte liée à une condition de performance

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition de compteurs de performances personnalisés

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

## Définition d'une alerte par rapport à un compteur personnalisé

### Script

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
```

### Tests

#### Tableau de tests

\mbox{}

Vous trouverez ci-dessous le tableau des tests effectués.

| Test | Résultat |
| :------------- | :------------- |
| Affichage de la configuration d'Apache | OK |
| Vérification de la syntaxe | OK |
| Application des changements de la configuration | OK |
| Accès au site par défaut via un poste client | OK |

#### Bilan des tests

\mbox{}

Les tests effectués ont permis de démontrer le bon fonctionnement des commandes effectuées ainsi que leur maitrise par le technicien.

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-xml-index-transact-sql, consulté le 13 février 2018

http://use-the-index-luke.com/fr/sql/regrouper-les-donnees/parcours-d-index-couvrants, consulté le 13 férier 2018

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/new-localuser?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/new-localgroup?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/add-localgroupmember?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/sql/ssms/agent/create-an-operator, consulté le 12 mars 2018

https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-add-operator-transact-sql, consulté le 13 mars 2018

\newpage

# Annexes

## Fichier SQL

Vous trouverez ci-dessous le contenu du fichier sql permettant de réaliser le laboratoire.

```sql
-- Author: Cédric Martinez
-- Copyright March 2018
-- DBM1 Laboratoire 4
-- Partie 1 Définition de nouveaux opérateurs

-- Partie 2 Définition d'une alerte par rapport à un numéro d'erreur

-- Partie 3 Définition d'une alerte par rapport à une gravité d'erreur

-- Partie 4 Définition d'un nouveau travail

-- Partie 5 Planification de l'exécution d'un travail

-- Partie 6 Notification d'un opérateur suite à l'exécution du travail

-- Partie 7 Exécution d'un travail en réponse à une alerte

-- Partie 8 Définition d'une alerte liée à une condition de performance

-- Partie 9 Définition de compteurs de performances personnalisés

-- Partie 10 Définition d'une alerte par rapport à un compteur personnalisé

```
