-- Creation d'une base de données
USE MASTER;
GO
CREATE DATABASE "TSQL"
ON
( NAME = TSQL_dat,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL.mdf',
SIZE = 5MB,
FILEGROWTH = 5MB )
LOG ON
( NAME = TSQL_log,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_log.ldf',
SIZE = 2MB,
FILEGROWTH = 2MB );
GO

-- Définition d'un groupe de fichiers
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILEGROUP "TPTSQL";
GO

-- Ajout des fichiers de données
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TSQL_2,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPTSQL;
GO

-- Ajout d'un fichier journal
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD LOG FILE
(
  NAME = TSQL_2_Log,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2_Log.ldf',
  SIZE = 10 MB,
  MAXSIZE = 10 MB
);
GO

-- Modification d'un fichier de données
USE "TSQL";
ALTER DATABASE "TSQL"
MODIFY FILE
(
  NAME = TSQL_2,
  MAXSIZE = UNLIMITED
);

-- Réduction de la taille d'un fichier de données
USE TSQL;
GO
DBCC SHRINKFILE (TSQL_2, 2);
GO

-- Réduction de la taille d'une base de données
USE TSQL;
GO
DBCC SHRINKDATABASE (TSQL, 10);
GO

-- Création d'une table sur un groupe de fichiers SSMS
USE "SSMS";
GO
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = TPSSMS_1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSSMS_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSSMS;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPSSMS;

-- Création d'une table sur un groupe de fichiers TSQL
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPTSQL;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPTSQL;

-- Création d'une table partitionnée SSMS
USE "SSMS";
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "SSMS" ADD FILEGROUP "IDF";
ALTER DATABASE "SSMS" ADD FILEGROUP "CN";
ALTER DATABASE "SSMS" ADD FILEGROUP "NE";
ALTER DATABASE "SSMS" ADD FILEGROUP "O";
ALTER DATABASE "SSMS" ADD FILEGROUP "SO";
ALTER DATABASE "SSMS" ADD FILEGROUP "SE";
ALTER DATABASE "SSMS" ADD FILEGROUP "S";
GO
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = IDF1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP IDF;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = CN1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP CN;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = NE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP NE;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = O1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP O;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = SO1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP SO;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = SE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP SE;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = S1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP S;
GO
CREATE PARTITION FUNCTION region_Function (int)
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);
GO
CREATE PARTITION SCHEME Region_Scheme
    AS PARTITION region_Function
    TO (IDF,CN,NE,O,SO,SE,S);
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO
exec sp_help CLIENTS;

-- Création d'une table partitionnée TSQL
USE "TSQL";
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_IDF";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_CN";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_NE";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_O";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SO";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SE";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_S";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_IDF1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_IDF;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_CN1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_CN;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_NE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_NE;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_O1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_O;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_SO1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_SO;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_SE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_SE;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_S1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_S;
GO
CREATE PARTITION FUNCTION region_Function (int)
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);
GO
CREATE PARTITION SCHEME Region_Scheme
    AS PARTITION region_Function
    TO (TPSQL_IDF,TPSQL_CN,TPSQL_NE,TPSQL_O,TPSQL_SO,TPSQL_SE,TPSQL_S);
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO
exec sp_help CLIENTS;

-- Compression de données SSMS
USE "SSMS";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
ALTER TABLE ARTICLES REBUILD PARTITION = ALL
  WITH (DATA_COMPRESSION = ROW);
GO

-- Compression de données TSQL
USE "TSQL";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
ALTER TABLE ARTICLES REBUILD PARTITION = ALL
  WITH (DATA_COMPRESSION = ROW);
GO

-- Mise en place d'index SSMS
USE "SSMS";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
GO
CREATE UNIQUE INDEX idx ON produits (id);

-- Mise en place d'index TSQL
USE "TSQL";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
CREATE UNIQUE INDEX idx ON produits (id);

--Indexation d'une colonne de type xml SSMS
USE "SSMS";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);
GO

-- Indexation d'une colonne de type xml TSQL
USE "TSQL";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);

-- Définition d'un index couvrant SSMS
USE "SSMS";
CREATE INDEX index_libelle_marque
    ON produits
     ( libelle, marque );

-- Définition d'un index couvrant TSQL
USE "TSQL";
CREATE INDEX index_libelle_marque
     ON produits
      ( libelle, marque );
