---
title: DBM1 - Documentation SQL Partie 2
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\sql-server.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify


# Questionnaire

## Combien de fichiers sont nécessaires pour la création de la base de données ?

A. 1 fichier
B. 2 fichiers
C. 3 fichiers
D. 4 fichiers


### Réponse

B

## Quel est le rôle de la base Model ?

### Réponse

Servir de modèle lors de la création des bases de données, définir les paramètres par défaut.

## Si des modifications sont apportées à la base Model, qui va en bénéficier ?

A. Toutes les bases
B. Uniquement les bases utilisateurs
C. Les bases qui sont créées par la suite

### Réponse

C

## Quels sont les groupes de fichiers créés par défaut sur une base de données ?

A. Default
B. System
C. Primary
D. Users
E. Temp

### Réponse

C

## Quel est le nom du groupe de fichiers utilisé par les tables systèmes ?

A. System
B. Primary
C. Default

### Réponse

B

## Est-il possible de définir une table sur un fichier précis ?

A. Oui
B. Non

### Réponse

B

## Dans quel sens est-il possible de partitionner une table ?

A. Verticalement
B. Horizontalement

### Réponse

B

## Combien de fichiers, contient, au minimum un groupe de fichiers ?

A. 0
B. 1
C. 2

### Réponse

A

## Est-il possible de déplacer un fichier d'un groupe de fichiers vers un autre ?

A. Oui
B. Oui, à l'exception des fichiers qui appartiennent au groupe PRIMARY
C. Non

### Réponse

C

## Lorsque l'option AUTO_SHRINK est activée, à partir de quelle quantité d'espace libre le fichier est-il réduit de façon automatique ?

A. 10 % d'espace libre
B. 20% d'espace libre
C. 25 % d'espace libre
D. 33% d'espace libre
E. Le pourcentage d'espace libre est à fixer lorsque l'option AUTO_SHRINK est activée


### Réponse

C

## Le fichier principal de données ppal.mdf a été créé en même temps que la base avec une taille initiale de 10 Mo. Ce fichier est paramétré en croissance automatique. Des tables utilisateurs ont été définies sur le groupe de fichiers PRIMARY, tant et si bien que le fichier a atteint une taille de 20 Mo. Les tables utilisateurs sont supprimées. Le fichier ne contient plus que 4 Mo d'information. Le paramètre AUTO_SHRINK est activé sur la base de données. Quelle est la taille du fichier ppal.mdf après que le processus de réduction automatique a été exécuté ?

A. 20 Mo
B. 10 Mo
C. 4 Mo

### Réponse

B

## Sur une table de base de données combien d'index non-c1ustered est-il possible de définir ?

A. Aucun car ce type d'index est réservé aux vues
B. 1 seul index de ce type peut être défini sur chaque table
C. 249 à condition qu'une clé primaire soit définie sur la table
D. 249 qu'il y ait ou pas de clé primaire

### Réponse

D

## Pour être en mesure de définir un index sur une colonne de type xml, la table doit-elle obligatoirement disposer d'une contrainte de clé primaire ?

A. Oui
B. Non un index unique est suffisant
C. Non le mécanisme d'indexation est indépendant de celui des contraintes d'intégrité

### Réponse

A

## La compression des données est :

A. Configurable au niveau de la base
B. Configurable au niveau des groupes de fichiers
C. Configurable au niveau de chaque table
D. Disponible uniquement pour les sauvegardes

### Réponse

C

\newpage

# Laboratoire

## Creation d'une base de données

### SSMS

Afin de créer une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier Databases et cliquer sur New Database.

Ensuite, il faut saisir le nom de la base de données SSMS dans le champ Name et cliquer sur le bouton OK.

### Script

```sql
USE MASTER;
CREATE DATABASE "TSQL"
ON
( NAME = TSQL_dat,  
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL.mdf',  
SIZE = 5MB,   
FILEGROWTH = 5MB )  
LOG ON  
( NAME = TSQL_log,  
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_log.ldf',  
SIZE = 2MB,   
FILEGROWTH = 2MB );
```

## Définition d'un groupe de fichiers

Afin de définir un groupe de fichiers en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Filegroups.

Après cela, il faut cliquer sur le bouton Add situé en dessous de Rows.

Il faut ensuite saisir le nom du filegroupe désiré ici TPSSMS et cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER DATABASE "TSQL"
ADD FILEGROUP "TPTSQL";   
```

## Ajout des fichiers de données

Afin d'ajouter des fichiers de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Files.

Puis, il faut cliquer sur le bouton Add.

Après cela, il faut saisir SSMS_2.ndf dans le champ Logical Name, saisir 5 dans le champ Initial Size et cliquer sur le bouton ... situé dans la colonne Maxsize.

Dans la fenêtre qui s'ouvre, il faut cliquer sur le radio-bouton nommé In Megabytes en dessous de File Growth, saisir 5, puis cliquer sur le radio-bouton nommé Limited to (MB) en dessous de Maximum File Size, saisir 5 et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TSQL_2,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPTSQL;
```

## Ajout d'un fichier journal

Afin d'ajouter un fichier journal en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Files.

Puis, il faut cliquer sur le bouton Add.

Après cela, il faut saisir SSMS_2_Log dans le champ Logical Name, sélectionner LOG dans le champ File Type, saisir 5 dans le champ Initial Size et cliquer sur le bouton ... situé dans la colonne Maxsize.

Dans la fenêtre qui s'ouvre, il faut décocher la case à cocher nommée Enable AutoGrowth et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER DATABASE "TSQL"
ADD LOG FILE
(  
  NAME = TSQL_2_Log,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2_Log.ldf',
  SIZE = 10 MB,
  MAXSIZE = 10 MB
);
```

## Modification d'un fichier de données

Afin de modifier un fichier de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet File.

Puis, il faut saisir 50 dans le champ Initial Size à la ligne contenant SSMS dans le champ File Name et cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER DATABASE "TSQL"
MODIFY FILE
(
  NAME = TSQL_2,
  MAXSIZE = UNLIMITED
);
```

## Réduction de la taille d'un fichier de données

Afin de réduire la taille d'un fichier de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS, sélectionner Tasks, puis Shrink et enfin Files.

Après cela, il faut sélectionner SSMS dans la liste déroulante File name.

Ensuite, il faut cliquer sur le radio bouton Reorganize pages before releasing unused space et saisir 45 dans le champ Shrink file to.

Puis, il faut cliquer sur le bouton OK.

### Script

```sql
USE TSQL;  
GO  
DBCC SHRINKFILE (TSQL_2, 2);  
GO  
```

## Réduction de la taille d'une base de données

Afin d'activer la réduction automatique de la taille d'une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Options.

Après cela, il faut sélectionner True dans le champ AutoShrink et cliquer sur le bouton OK.

### Script

```sql
USE TSQL;  
GO
DBCC SHRINKDATABASE (TSQL, 10);  
GO  
```

## Création d'une table sur un groupe de fichiers

Afin de créer une table sur un groupe de fichiers dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner New Query.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";
GO
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = TPSSMS_1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSSMS_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSSMS;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPSSMS;
```

### Script

```sql
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPTSQL;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPTSQL;
```

## Création d'une table partitionnée

Afin de créer une table partitionnée dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner SQL Request.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";  
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "SSMS" ADD FILEGROUP "IDF";  
ALTER DATABASE "SSMS" ADD FILEGROUP "CN";    
ALTER DATABASE "SSMS" ADD FILEGROUP "NE";    
ALTER DATABASE "SSMS" ADD FILEGROUP "O";     
ALTER DATABASE "SSMS" ADD FILEGROUP "SO";
ALTER DATABASE "SSMS" ADD FILEGROUP "SE";
ALTER DATABASE "SSMS" ADD FILEGROUP "S";  
GO
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = IDF1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP IDF;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = CN1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP CN;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = NE1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP NE;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = O1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP O;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = SO1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP SO;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = SE1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP SE;
ALTER DATABASE "SSMS"
ADD FILE
(  
  NAME = S1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP S;
GO
CREATE PARTITION FUNCTION region_Function (int)  
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);  
GO  
CREATE PARTITION SCHEME Region_Scheme  
    AS PARTITION region_Function  
    TO (IDF,CN,NE,O,SO,SE,S);  
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO  
exec sp_help CLIENTS;
```

### Script

```sql
USE "TSQL";  
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_IDF";  
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_CN";    
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_NE";    
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_O";     
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SO";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SE";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_S";  
GO
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_IDF1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_IDF;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_CN1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_CN;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_NE1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_NE;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_O1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_O;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_SO1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_SO;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_SE1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_SE;
ALTER DATABASE "TSQL"
ADD FILE
(  
  NAME = TPSQL_S1,  
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,   
  FILEGROWTH = 2 MB
)  
TO FILEGROUP TPSQL_S;
GO
CREATE PARTITION FUNCTION region_Function (int)  
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);  
GO  
CREATE PARTITION SCHEME Region_Scheme  
    AS PARTITION region_Function  
    TO (TPSQL_IDF,TPSQL_CN,TPSQL_NE,TPSQL_O,TPSQL_SO,TPSQL_SE,TPSQL_S);  
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO  
exec sp_help CLIENTS;
```

## Compression de données

Afin de compresser les données dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner SQL Request.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
ALTER TABLE ARTICLES REBUILD PARTITION = ALL  
  WITH (DATA_COMPRESSION = ROW);   
GO
```

### Script

```sql
USE "TSQL";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
  ALTER TABLE ARTICLES REBUILD PARTITION = ALL  
    WITH (DATA_COMPRESSION = ROW);   
  GO
```

## Mise en place d'index

Afin de mettre en place un index dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner SQL Request.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
GO
CREATE UNIQUE INDEX idx ON produits (id);
```

### Script

```sql
USE "TSQL";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
CREATE UNIQUE INDEX idx ON produits (id);
```

## Indexation d'une colonne de type xml

Afin de mettre en place un index XML dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner SQL Request.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);
GO
```

### Script

```sql
USE "TSQL";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);
```

## Définition d'un index couvrant

```sql
USE "SSMS";
SELECT libelle, marque
FROM produits
WHERE libelle like 'cri tere% ' ;
```

Afin de mettre en place un index couvrant la requête ci-dessus dans une base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner SQL Request.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
USE "SSMS";
CREATE INDEX index_libelle_marque
    ON produits
     ( libelle, marque );
```

### Script

```sql
USE "TSQL";
CREATE INDEX index_libelle_marque
    ON produits
     ( libelle, marque );
```


\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-database-sql-server-transact-sql, consulté le 8 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/alter-database-transact-sql-file-and-filegroup-options, consulté le 8 février 2018

https://docs.microsoft.com/en-us/sql/relational-databases/partitions/create-partitioned-tables-and-indexes, consulté le 13 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-index-transact-sql, consulté le 13 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-xml-index-transact-sql, consulté le 13 février 2018

http://use-the-index-luke.com/fr/sql/regrouper-les-donnees/parcours-d-index-couvrants, consulté le 13 férier 2018

\newpage

# Annexes

## Fichier sql

Vous trouverez ci-dessous le contenu du fichier sql permettant de réaliser le laboratoire.

```sql
-- Creation d'une base de données
USE MASTER;
GO
CREATE DATABASE "TSQL"
ON
( NAME = TSQL_dat,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL.mdf',
SIZE = 5MB,
FILEGROWTH = 5MB )
LOG ON
( NAME = TSQL_log,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_log.ldf',
SIZE = 2MB,
FILEGROWTH = 2MB );
GO

-- Définition d'un groupe de fichiers
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILEGROUP "TPTSQL";
GO

-- Ajout des fichiers de données
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TSQL_2,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPTSQL;
GO

-- Ajout d'un fichier journal
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD LOG FILE
(
  NAME = TSQL_2_Log,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TSQL_2_Log.ldf',
  SIZE = 10 MB,
  MAXSIZE = 10 MB
);
GO

-- Modification d'un fichier de données
USE "TSQL";
ALTER DATABASE "TSQL"
MODIFY FILE
(
  NAME = TSQL_2,
  MAXSIZE = UNLIMITED
);

-- Réduction de la taille d'un fichier de données
USE TSQL;
GO
DBCC SHRINKFILE (TSQL_2, 2);
GO

-- Réduction de la taille d'une base de données
USE TSQL;
GO
DBCC SHRINKDATABASE (TSQL, 10);
GO

-- Création d'une table sur un groupe de fichiers SSMS
USE "SSMS";
GO
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = TPSSMS_1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSSMS_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSSMS;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPSSMS;

-- Création d'une table sur un groupe de fichiers TSQL
USE "TSQL";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPTSQL;
GO
CREATE TABLE CLIENTS (
  numero INT CONSTRAINT pk_clients PRIMARY KEY,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50)
  )
ON TPTSQL;

-- Création d'une table partitionnée SSMS
USE "SSMS";
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "SSMS" ADD FILEGROUP "IDF";
ALTER DATABASE "SSMS" ADD FILEGROUP "CN";
ALTER DATABASE "SSMS" ADD FILEGROUP "NE";
ALTER DATABASE "SSMS" ADD FILEGROUP "O";
ALTER DATABASE "SSMS" ADD FILEGROUP "SO";
ALTER DATABASE "SSMS" ADD FILEGROUP "SE";
ALTER DATABASE "SSMS" ADD FILEGROUP "S";
GO
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = IDF1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP IDF;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = CN1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP CN;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = NE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP NE;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = O1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP O;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = SO1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP SO;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = SE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP SE;
ALTER DATABASE "SSMS"
ADD FILE
(
  NAME = S1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP S;
GO
CREATE PARTITION FUNCTION region_Function (int)
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);
GO
CREATE PARTITION SCHEME Region_Scheme
    AS PARTITION region_Function
    TO (IDF,CN,NE,O,SO,SE,S);
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO
exec sp_help CLIENTS;

-- Création d'une table partitionnée TSQL
USE "TSQL";
GO
DROP TABLE CLIENTS;
GO
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_IDF";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_CN";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_NE";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_O";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SO";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_SE";
ALTER DATABASE "TSQL" ADD FILEGROUP "TPSQL_S";
GO
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_IDF1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_IDF1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_IDF;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_CN1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_CN1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_CN;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_NE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_NE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_NE;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_O1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_O1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_O;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_SO1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SO1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_SO;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_SE1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_SE1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_SE;
ALTER DATABASE "TSQL"
ADD FILE
(
  NAME = TPSQL_S1,
  FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.INSTANCETP\MSSQL\DATA\TPSQL_S1.ndf',
  SIZE = 5 MB,
  MAXSIZE = 10 MB,
  FILEGROWTH = 2 MB
)
TO FILEGROUP TPSQL_S;
GO
CREATE PARTITION FUNCTION region_Function (int)
    AS RANGE LEFT FOR VALUES (1,2,3,4,5,6);
GO
CREATE PARTITION SCHEME Region_Scheme
    AS PARTITION region_Function
    TO (TPSQL_IDF,TPSQL_CN,TPSQL_NE,TPSQL_O,TPSQL_SO,TPSQL_SE,TPSQL_S);
GO
CREATE TABLE CLIENTS (
  numero INT NOT NULL,
  nom   NVARCHAR(50) NOT NULL,
  prenom NVARCHAR(50) NOT NULL,
  adresse NVARCHAR(100),
  codepostal CHAR(5),
  ville NVARCHAR(50),
  region INT NOT NULL,
  CONSTRAINT pl_clients PRIMARY KEY (numero, region)
  ) ON Region_Scheme(region);
GO
exec sp_help CLIENTS;

-- Compression de données SSMS
USE "SSMS";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
ALTER TABLE ARTICLES REBUILD PARTITION = ALL
  WITH (DATA_COMPRESSION = ROW);
GO

-- Compression de données TSQL
USE "TSQL";
GO
CREATE TABLE ARTICLES (
  refart char(10) constraint pk_articles primary key,
  designation nvarchar(100),
  prixht money
  );
ALTER TABLE ARTICLES REBUILD PARTITION = ALL
  WITH (DATA_COMPRESSION = ROW);
GO

-- Mise en place d'index SSMS
USE "SSMS";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
GO
CREATE UNIQUE INDEX idx ON produits (id);

-- Mise en place d'index TSQL
USE "TSQL";
GO
CREATE TABLE produits (
  id int identity(1,1) constraint pk_produits primary key,
  libelle nvarchar(200),
  description xml,
  categorie varchar(10),
  marque varchar(50)
  );
CREATE UNIQUE INDEX idx ON produits (id);

--Indexation d'une colonne de type xml SSMS
USE "SSMS";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);
GO

-- Indexation d'une colonne de type xml TSQL
USE "TSQL";
ALTER TABLE produits (
  id int identity(1,1) constraint pk_produits primary key
  );
GO
CREATE PRIMARY XML INDEX idxml ON produits (description);

-- Définition d'un index couvrant SSMS
USE "SSMS";
CREATE INDEX index_libelle_marque
    ON produits
     ( libelle, marque );

-- Définition d'un index couvrant TSQL
USE "TSQL";
CREATE INDEX index_libelle_marque
     ON produits
      ( libelle, marque );

```
