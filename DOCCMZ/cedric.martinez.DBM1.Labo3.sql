-- Partie 1 Configurer le mode de sécurité
USE "master";
ALTER LOGIN "sa" ENABLE;
GO
ALTER LOGIN "sa" WITH PASSWORD=N'Qaywsx123456';
GO
-- Partie 2 Définir une nouvelle connexion
USE "TSQL";
CREATE LOGIN "Pierre"
    WITH PASSWORD = 'Qaywsx123456',
    CHECK_POLICY = OFF,
    DEFAULT_DATABASE = TSQL;
GO
CREATE USER "Pierre" FOR LOGIN "Pierre";
GO
-- Partie 3 Intégrer des comptes et groupes Windows
USE "TSQL";
CREATE LOGIN "HOSTNAME\Léon"
FROM WINDOWS WITH DEFAULT_DATABASE = "TSQL";
GO
-- Partie 4 Définir des utilisateurs de base de données
USE "TSQL";
CREATE USER "Léon" FOR LOGIN "HOSTNAME\Léon";
GO
-- Partie 5 Travailler avec le compte guest
USE "TSQL";
ALTER LOGIN "HOSTNAME\Léon" ENABLE;
ALTER LOGIN "guest" ENABLE;
GO
-- Partie 6 Accorder des droits spécifiques
USE "TSQL";
GRANT CREATE TABLE, CREATE VIEW, CREATE PROCEDURE TO "Léon";
-- Partie 7 Définir un rôle de base de données
USE "TSQL";
CREATE ROLE "roleTSQL";
GRANT CREATE TABLE,CREATE VIEW,CREATE PROCEDURE TO "roleTSQL";
-- Partie 8 Accorder des rôles de bases de données à des utilisateurs
USE "TSQL";
ALTER ROLE "db_datareader" ADD MEMBER "Léon";
GO
ALTER ROLE "db_datawriter" ADD MEMBER "guest";
ALTER ROLE "roleTSQL" ADD MEMBER "guest";
GO
-- Partie 9 Interdire l'utilisation de certains privilèges
USE "TSQL";
DENY CREATE PROCEDURE TO "Léon";
DENY CREATE FUNCTION TO "Léon";
-- Partie 10 Définir un rôle d'application
USE "SSMS";
GRANT CREATE TABLE, CREATE VIEW TO "roleApplication";
-- Partie 11 Utiliser un rôle d'application
USE "SSMS";
ALTER APPLICATION ROLE "roleApplication" WITH DEFAULT_SCHEMA = "db_owner";
EXECUTE AS LOGIN='DBM1-SQL1\Léon';
USE "SSMS";
exec sp_setapprole @rolename='roleApplication',@password='SSM$123456789';
CREATE TABLE TTEST (C1 int);
