---
title: DBM1 - Documentation SQL Partie 3
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\sql-server.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Questionnaire

## Les utilisateurs sont définis au niveau :

A. Serveur
B. Base de données
C. Tables

### Réponse

A

## Pour travailler sur une base de données, est-il nécessaire de posséder un compte utilisateur nominatif ?

A. Oui.
B. Non.


### Réponse

A

## À quoi servent les rôles ?

A. Définir des groupes de connexions
B. Définir des groupes d'utilisateurs
C. Définir des groupes de privilèges


### Réponse

C

## Quel(s) type(s) de rôle est-il possible d'accorder à un utilisateur ?

A. Rôle de serveur
B. Rôle de base de données
C. Rôle personnalisé
D. Rôle d'application


### Réponse

B et D.

## Quelle instruction faut-il utiliser pour enlever un privilège à un utilisateur ?

A. GRANT.
B. REVOKE.
C. DENY.


### Réponse

B

## À quel niveau est-il possible de définir ses propres rôles ?

A. Serveur et base de données
B. Serveur
C. Base de données
D. Ce n'est pas possible


### Réponse

C

## Quel rôle permet d'accorder les privilèges d'administration d'une base ?

A. Sysadmin
B. db_admin
C. dbo
D. db_owner

### Réponse

B

## En tant qu'administrateur du serveur sur lequel s'exécute l'instance SQL server, de quels privilèges je dispose par défaut au niveau de SQL Server ?

A. Aucun
B. DiskAdmin
C. ServerAdmin
D. SysAdmin


### Réponse

D


## Est-il possible d'utiliser un rôle d'application depuis un script Transact SQL ?

A. Oui
B. Non


### Réponse

A

## Quel type de privilège peut être accordé à un rôle d'application ?

A. Privilèges systèmes
B. Privilèges objets
C. Rôles de base de données
D. Rôles de serveur

### Réponse

B

## Quel est le rôle du compte d'utilisateur de base de données guest ?

### Réponse

Permettre de voir le contenu d'une base de données.

## Si aucun schéma n'est associé à l'utilisateur de base de données, quel sera son schéma par défaut ?

A. PUBLIC
B. DBO
C. DB_OWNER
D. Le schéma par défaut est celui qui porte le nom de l'utilisateur


### Réponse

C

\newpage

# Laboratoire

## Configuration du mode de sécurité

### Activation du mode d'authentification SQL et Windows

Afin d'activer le mode d'authentification SQL et Windows en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur l'icône représentant le serveur SQL Server et sélectionner Properties.

Ensuite, il faut cliquer sur le lien Security du menu de gauche.

Après cela, il faut cliquer sur le radio-bouton nommé SQL Server and Windows Authentication mode et cliquer sur le bouton OK.

Finalement, il faut cliquer-droit sur l'icone représentant le serveur SQL et sélectionner Restart pour redémarrer le serveur SQL.

### Définition du mot de passe de l'utilisateur sa

Afin de définir le mot de passe de l'utilisateur sa en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier Security, puis sur le dossier Logins et sur l'utilisateur sa et sélectionner Properties.

Dans la fenêtre qui s'ouvre, il faut saisir Qaywsx123456 dans le champ Password et Confirm Password.

Après, il faut cliquer sur le lien Status du menu de gauche.

Ensuite, il faut cliquer sur le radio-bouton Enable en dessous de Login.

Finalement, il faut cliquer sur le bouton OK.

### Script

```sql
USE "master";
ALTER LOGIN sa ENABLE;
GO
ALTER LOGIN sa WITH PASSWORD=N'Qaywsx123456';
GO
```

\newpage

## Définition d'une nouvelle connexion

Afin de définir un nouvel utilisateur en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier Security, puis sur le dossier Logins et sélectionner New login.

Il faut ensuite saisir Adrien dans le champ Name.

Puis, il faut sélectionner le radio-bouton SQL Server Authentication.

ENsuite, il faut saisir le mot de passe Qaywsx123456 dans le champ Password et Confirm Password.

Après cela, il faut selectionner SSMS dans la liste déroulante default database.

Puis, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
CREATE LOGIN "Pierre"   
    WITH PASSWORD = 'Qaywsx123456',
    CHECK_POLICY = OFF,
    DEFAULT_DATABASE = TSQL;  
GO  
CREATE USER Pierre FOR LOGIN Pierre;  
GO  
```

\newpage

## Intégration de groupes Windows

### Création des utilisateurs

```Powershell
New-LocalUser "Géraldine" -Password (Read-Host -AsSecureString "Qaywsx123456")

-FullName "Géraldine" -Description "Utilisatrice Géraldine"
```

```Powershell
New-LocalUser "René" -Password (Read-Host -AsSecureString "Edcrfv123456")
-FullName "René" -Description "Utilisateur René"
```

```Powershell
New-LocalUser "Léon" -Password (Read-Host -AsSecureString "Tgbzhn123456")
-FullName "Léon" -Description "Utilisateur Léon"
```

### Création des groupes Windows

```Powershell
New-LocalGroup -Name "Groupe1_TPSQL"
```

```Powershell
New-LocalGroup -Name "Groupe2_TPSQL"
```

### Ajout des utilisateurs dans les groupes

```Powershell
Add-LocalGroupMember -Group "Groupe1_TPSQL" -Member "Géraldine", "Léon"
```

```Powershell
Add-LocalGroupMember -Group "Groupe2_TPSQL" -Member "René", "Léon"
```

### Définition de la connexion de Géraldine

Afin de définir la connexion de Géraldine en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier Security, puis sur le dossier Logins et sélectionner New login.

Il faut ensuite saisir HOSTNAME\\Géraldine dans le champ Name.

Après cela, il faut selectionner SSMS dans la liste déroulante default database.

Puis, il faut cliquer sur le bouton OK.

\newpage

### Autorisation de groupe Groupe2_TPSQL

Afin d'autoriser le groupe Groupe2_TSQL en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier Security, puis cliquer-droit sur le dossier Logins et sélectionner New login.

Après, il faut cliquer sur le bouton Search.

Ensuite, il faut cliquer sur le bouton Object Types, cocher Groups et cliquer sur le bouton OK.

Il faut ensuite saisir HOSTNAME\\Groupe2_TSQL dans le champ Enter the object name to select et cliquer sur le bouton OK.

Après cela, il faut selectionner SSMS dans la liste déroulante default database.

Puis, il faut cliquer sur le bouton OK.


### Script

```sql
USE "TSQL";
CREATE LOGIN "HOSTNAME\Léon"
FROM WINDOWS WITH DEFAULT_DATABASE = "TSQL";  
GO  
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Connexion à SSMS avec Geraldine | OK |
| Connexion à SSMS avec René | OK |
| Connexion à TSQL avec Léon | OK |
| Connexion à TSQL avec René | OK |

\newpage

## Définition d'utilisateurs de DB

Afin de définir l'utilisatrice Géraldine en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security et cliquer-droit sur Users et sélectionner New User.

Ensuite, il faut sélectionner Windows user dans le champ User type.

Après cela, il faut cliquer sur le bouton ... situé à droite du champ User name.

Dans la fenêtre qui s'ouvre il faut saisir Géraldine et cliquer sur le bouton Check Names.

Puis, il faut cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton ... situé à droite du champ Login name.

Après cela, il faut saisir Géraldine dans le champ Enter the object names to select et cliquer sur le bouton Browse.

Puis, il faut cocher la case à cocher située à gauche de Géraldine et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton OK pour fermer la fenêtre.

Finalement, il faut cliquer sur le bouton OK pour créer l'utilisatrice Géraldine dans la base de données.

### Script

```sql
USE "TSQL";
CREATE USER "Léon" FOR LOGIN "HOSTNAME\Léon";  
GO
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Connexion à TSQL avec Léon | OK |

\newpage

## Utilisation du compte guest

Afin d'activer l'utilisateur guest en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Permissions.

Après cela, il faut cliquer sur le bouton Search.

Puis, il faut saisir guest et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur l'utilisateur guest, puis cocher la case à cocher CONNECT à la colonne Grant.

Finalement, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER LOGIN "HOSTNAME\Léon" ENABLE;
ALTER LOGIN "guest" ENABLE;  
GO
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Connexion à SSMS avec Pierre | OK |
| Connexion à TSQL avec Pierre | OK |

\newpage

## Accord de droits spécifiques

Afin d'accorder des droits spécifiques à l'utilisatrice Géraldine en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Permissions.

Après cela, il faut cliquer sur le bouton Search.

Puis, il faut saisir DBM1-SQL1\\Geraldine et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur l'utilisateur DBM1-SQL1\\Geraldine, puis cocher la case à cocher CREATE TABLE et CREATE VIEW à la colonne Grant.

Finalement, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
GRANT CREATE TABLE, CREATE VIEW, CREATE PROCEDURE TO "Léon";
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Création d'une table sur TSQL avec Léon | OK |
| Création d'une vue sur TSQL avec Léon | OK |

\newpage

## Définition d'un rôle de base de données

Afin de définir un rôle de base de données en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security et cliquer-droit sur Roles et sélectionner New Database Role.

Ensuite, il faut saisir le nom du rôle dans le champ Role name ici roleSSMS et cliquer sur le bouton OK.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Permissions.

Après cela, il faut cliquer sur le bouton Search.

Puis, il faut saisir roleSSMS et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le rôle roleSSMS, puis cocher la case à cocher CREATE TABLE, CREATE PROCEDURE et CREATE VIEW à la colonne Grant.

Finalement, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
CREATE ROLE "roleTSQL";
GRANT CREATE TABLE, CREATE VIEW, CREATE PROCEDURE TO "roleTSQL";
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Création d'une table sur TSQL avec roleTSQL | OK |
| Création d'une vue sur TSQL avec roleTSQL | OK |

\newpage

## Accord de droits de bases de données à des utilisateurs

Afin d'accorder des droits de base de données à l'utilisatrice Géraldine en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security, puis sur Roles et cliquer-droit sur le rôle db_owner et sélectionner Properties.

Ensuite, il faut cliquer sur le bouton Add.

Puis, il faut saisir DBM1-SQL1\\Geraldine et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Finalement, il faut cliquer sur le bouton OK.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security, puis sur Roles et cliquer-droit sur le rôle db_datareader et sélectionner Properties.

Ensuite, il faut cliquer sur le bouton Add.

Puis, il faut saisir guest et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Finalement, il faut cliquer sur le bouton OK.

Afin de définir l'utilisateur Groupe2_TPSQL utilisant la connexion Groupe2_TPSQL en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security et cliquer-droit sur Users et sélectionner New User.

Ensuite, il faut sélectionner SQL user with login dans le champ User type.

Après cela, il faut saisir Groupe2_TPSQL dans le champ Username.

Ensuite, il faut cliquer sur le bouton ... situé à droite du champ Login name.

Après cela, il faut saisir DBM1-SQL1\\Groupe2_TPSQL dans le champ Enter the object names to select et cliquer sur le bouton Browse.

Puis, il faut cocher la case à cocher située à gauche de DBM1-SQL1\\Groupe2_TPSQL et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton OK pour fermer la fenêtre.

Finalement, il faut cliquer sur le bouton OK pour créer l'utilisateur Groupe2_TPSQL dans la base de données.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security, puis sur Roles et cliquer-droit sur le rôle db_datareader et sélectionner Properties.

Ensuite, il faut cliquer sur le bouton Add.

Puis, il faut saisir Groupe2_TPSQL et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Finalement, il faut cliquer sur le bouton OK.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security, puis sur Roles et cliquer-droit sur le rôle roleSSMS et sélectionner Properties.

Ensuite, il faut cliquer sur le bouton Add.

Puis, il faut saisir Groupe2_TPSQL et cliquer sur le bouton Check names et cliquer sur le bouton OK.

Finalement, il faut cliquer sur le bouton OK.

### Script

```sql
USE "TSQL";
ALTER ROLE "db_datareader" ADD MEMBER "Léon";
GO
ALTER ROLE "db_datawriter" ADD MEMBER "guest";
ALTER ROLE "roleTSQL" ADD MEMBER "guest";
GO
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Affichage des tables avec Léon | OK |
| Affichage des tables avec guest | OK |

\newpage

## Interdiction de certains privilèges

Afin d'interdire certains privilièges à un groupe d'utilisateurs en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner Properties.

Ensuite, il faut cliquer sur l'onglet Permissions.

Ensuite, il faut cliquer sur l'utilisateur Groupe2_TPSQL, puis cocher la case à cocher CREATE TABLE, CREATE PROCEDURE à la colonne Deny.

Finalement, il faut cliquer sur le bouton OK.


### Script

```sql
USE "TSQL";
DENY CREATE PROCEDURE TO "Léon";
DENY CREATE FUNCTION TO "Léon";
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Création d'une procédure sur TSQL avec Léon | KO |
| Création d'une fonction sur TSQL avec Léon | KO |

\newpage

## Définition d'un rôle d'applications

Afin de définir un role d'application en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security et cliquer-droit sur Roles et sélectionner New Application Role.

Ensuite, il faut saisir le nom du rôle dans le champ Role name ici roleApplication, saisir le mot de passe SSM$123456789 dans le champ Password ainsi que dans le champ Confirm Password et cliquer sur le bouton OK.

### Script

```sql
USE "SSMS";
GRANT CREATE TABLE, CREATE VIEW TO "roleApplication";
```

### Tests

| Test | Résultat |
| :------------- | :------------- |
| Création d'une table sur SSMS avec roleApplication | OK |
| Création d'une vue sur SSMS avec roleApplication | OK |

\newpage

## Utilisation d'un rôle d'application

Afin d'utiliser un role d'application en utilisant SSMS, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL en spécifiant l'utilisateur DBM1-SQL\\Léon.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner New Query.

Ensuite, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
EXECUTE AS LOGIN='DBM1-SQL1\Léon';
USE "SSMS";
CREATE TABLE TTEST (C1 int);
```

La requête renvoie l'erreur "Cannot execute as the server principal because the principal DBM1-SQL1\\Léon doesn not exist, this type of principal cannot be impersonated or you do not have permission.".

Afin de corriger l'erreur, il faut activer le rôle roleApplication.

Pour ce faire, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL.

Puis, il faut cliquer sur le dossier contenant la base de données désirée ici SSMS, puis sur le dossier Security, puis Application Roles et cliquer-droit sur roleApplication et sélectionner Properties.

Il faut ensuite cléiquer sur le bouton ... situé à droite du champ Defualt schema.

Puis, il faut saisir db_owner et cliquer sur le bouton Check Names et cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton OK pour confirmer.

Afin de tester si l'erreur a été corrigée, il faut utiliser le role roleApplication pour exécuter le script.

Pour ce faire, il faut tout d'abord ouvrir SSMS et se connecter au serveur SQL avec l'utilisateur en spécifiant l'utilisateur DBM1-SQL\\Léon.

Puis, il faut cliquer-droit sur le dossier contenant la base de données désirée ici SSMS et sélectionner New Query.

Finalement, il faut coller le script ci-dessous et cliquer sur le bouton Execute.

```sql
EXECUTE AS LOGIN = 'DBM1-SQL1\Léon';
USE "SSMS";
exec sp_setapprole @rolename='roleApplication',@password='SSM$123456789';
CREATE TABLE TTEST (C1 int);
```

### Script

```sql
USE "SSMS";
ALTER APPLICATION ROLE "roleApplication" WITH DEFAULT_SCHEMA = "db_owner";
EXECUTE AS LOGIN='DBM1-SQL1\Léon';
USE "SSMS";
exec sp_setapprole @rolename='roleApplication',@password='SSM$123456789';
CREATE TABLE TTEST (C1 int);
```

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/new-localuser?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/new-localgroup?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/add-localgroupmember?view=powershell-5.1, consulté le 13 février 2018

https://docs.microsoft.com/en-us/sql/relational-databases/security/authentication-access/create-a-database-user, consulté le 13 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-user-transact-sql, consulté le 15 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-login-transact-sql, consulté le 15 février 2018

https://docs.microsoft.com/en-us/sql/relational-databases/security/authentication-access/create-a-login#TsqlProcedure, consulté le 15 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-role-transact-sql, consulté le 15 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-role-transact-sql, consulté le 15 février 2018

https://stackoverflow.com/questions/1136628/cannot-find-the-object-because-it-does-not-exist-or-you-do-not-have-permissions, consulté le 27 février 2018

https://docs.microsoft.com/en-us/sql/t-sql/statements/create-application-role-transact-sql, consulté le 27 février 2018

\newpage

# Annexes

## Fichier SQL

Vous trouverez ci-dessous le contenu du fichier sql permettant de réaliser le laboratoire.

```sql
-- Partie 1 Configurer le mode de sécurité
USE "master";
ALTER LOGIN "sa" ENABLE;
GO
ALTER LOGIN "sa" WITH PASSWORD=N'Qaywsx123456';
GO
-- Partie 2 Définir une nouvelle connexion
USE "TSQL";
CREATE LOGIN "Pierre"   
    WITH PASSWORD = 'Qaywsx123456',
    CHECK_POLICY = OFF,
    DEFAULT_DATABASE = TSQL;  
GO  
CREATE USER "Pierre" FOR LOGIN "Pierre";  
GO  
-- Partie 3 Intégrer des comptes et groupes Windows
USE "TSQL";
CREATE LOGIN "HOSTNAME\Léon"
FROM WINDOWS WITH DEFAULT_DATABASE = "TSQL";  
GO  
-- Partie 4 Définir des utilisateurs de base de données
USE "TSQL";
CREATE USER "Léon" FOR LOGIN "HOSTNAME\Léon";  
GO
-- Partie 5 Travailler avec le compte guest
USE "TSQL";
ALTER LOGIN "HOSTNAME\Léon" ENABLE;
ALTER LOGIN "guest" ENABLE;    
GO
-- Partie 6 Accorder des droits spécifiques
USE "TSQL";
GRANT CREATE TABLE, CREATE VIEW, CREATE PROCEDURE TO "Léon";
-- Partie 7 Définir un rôle de base de données
USE "TSQL";
CREATE ROLE "roleTSQL";
GRANT CREATE TABLE,CREATE VIEW,CREATE PROCEDURE TO "roleTSQL";
-- Partie 8 Accorder des rôles de bases de données à des utilisateurs
USE "TSQL";
ALTER ROLE "db_datareader" ADD MEMBER "Léon";
GO
ALTER ROLE "db_datawriter" ADD MEMBER "guest";
ALTER ROLE "roleTSQL" ADD MEMBER "guest";
GO
-- Partie 9 Interdire l'utilisation de certains privilèges
USE "TSQL";
DENY CREATE PROCEDURE TO "Léon";
DENY CREATE FUNCTION TO "Léon";
-- Partie 10 Définir un rôle d'application
USE "SSMS";
GRANT CREATE TABLE, CREATE VIEW TO "roleApplication";
-- Partie 11 Utiliser un rôle d'application
USE "SSMS";
ALTER APPLICATION ROLE "roleApplication" WITH DEFAULT_SCHEMA = "db_owner";
EXECUTE AS LOGIN='DBM1-SQL1\Léon';
USE "SSMS";
exec sp_setapprole @rolename='roleApplication',@password='SSM$123456789';
CREATE TABLE TTEST (C1 int);
```
